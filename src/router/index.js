import Vue from 'vue'
import VueRouter from "vue-router";
import AppDataTable from "../components/AppDataTable"
import DataDetail from "../components/DataDetail"
import LoginPage from "../components/LoginPage"

Vue.use(VueRouter);

function requireLogin(to, from, next) {
    if (localStorage.getItem('loggedIn')) {
        next();
    } else {
        next({
            path: "/login"
        });

    }
}


const routes = [{
        path: '/',
        name: 'list-employee',
        component: AppDataTable,
        beforeEnter: requireLogin
    },
    {
        path: '/employee/:id',
        name: 'detail-employee',
        component: DataDetail,
        props: true,
        beforeEnter: requireLogin
    },
    {
        path: '/login',
        name: 'login',
        component: LoginPage
    }

]

const router = new VueRouter({
    base: "/",
    routes,
    mode: "history"
});

export default router;